
db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
]);


// Find users with letter 's' in their 1st name letter 'd' lastame.


db.users.find( {$or: [ 
	{ firstName:{$regex: 'S'}},

	{ lastName: {$regex: 'D'}}
	]},
	{
		firstName: 1,
		lastName: 2,
		_id: 0
	}	
	
);


// HR age greater than or equal 70

db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}  ]});


// with letter e in their 1st name has an age of less than or equal to 30

db.users.find({$and: [{firstName: {$regex: 'E', $options: '$i'}}, {age: {$lte: 30}} ]});